import os
import random
import pathlib
import numpy as np
import pandas as pd
import seaborn as sns
import scipy.io as sio
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

def fetch_dataset():
    path_to_current_dir = os.getcwd()
    data_dir = os.path.join(path_to_current_dir, 'data')
    test_data = sio.loadmat(os.path.join(data_dir, 'test_data.mat'))
    train_data = sio.loadmat(os.path.join(data_dir, 'train_data.mat'))

    # Create a pandas dataframe from raw data
    raw_test_data = test_data['testdata']
    test_data = pd.DataFrame(raw_test_data).T
    test_data.columns = ["Label", "kettle", "fridge", "washer"]
    # This will assign each combination a label from 0 to 7 (2^3) so
    # we can treat the issues as categorical problem
    test_data["Label"] = test_data["Label"].astype('category').cat.codes

    raw_train_data = train_data['traindata']
    train_data = pd.DataFrame(raw_train_data).T
    train_data.columns = ["Label", "kettle", "fridge", "washer"]
    train_data["Label"] = train_data["Label"].astype('category').cat.codes

    # We want to predict label so remove it
    train_label = train_data.pop("Label")
    test_label = test_data.pop("Label")
    return train_data, test_data, train_label, test_label


def build_model(train_data):
    model = keras.Sequential([
        layers.Dense(32, activation='relu', input_shape=[len(train_data.keys())]),
        layers.Dense(64, activation='relu'),
        layers.Dense(8, activation="sigmoid")
    ])

    # For integer cross entropy (classes are 0..7) and not one-hot-encoded
    model.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  optimizer="Adam",
                  metrics=['accuracy'])
    return model


def main():
    EPOCHS = 300
    train_data, test_data, train_label, test_label = fetch_dataset()
    model = build_model(train_data)
    model.summary()

    early_stop = keras.callbacks.EarlyStopping(monitor='accuracy', patience=5)
    history = model.fit(
        train_data, train_label,
        epochs=EPOCHS, verbose=1,
        callbacks=[early_stop, tfdocs.modeling.EpochDots()]
    )
    
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch
    prediction = model.predict(test_data)

    # For some weird reason that I can not explain my results are very small at the output layer
    # However, when normalized between 1 and 0, we do always achieve the correct result
    # I am not sure where the issue lies.. So I created a normalizer function as I don't
    # have the time to debug the issue further
    def normalize_data(data):
        return (data - np.min(data)) / (np.max(data) - np.min(data))

    # Get the energy usage classes by getting the index of largest value in the predictions array
    # Also get last 100 from the test values to compare them
    # And we see that predictions are correct
    prediction_classes = np.array([np.argmax(x) for x in prediction[:100]]) #Normalize all values in predictions
    test_indices = (test_label[:100].to_numpy())

    ax = plt.axes()
    ax.plot(prediction_classes, "x", label="Electricity usage class for datapoint")
    ax.plot(test_data["kettle"][:100], "-", label="kettle")
    ax.plot(test_data["fridge"][:100], "-", label="fridge")
    ax.plot(test_data["washer"][:100], "-", label="washer")
    plt.legend()
    plt.show()
    return 1


if __name__ == "__main__":
    main()
