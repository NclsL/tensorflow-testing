import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

def generate_and_display_dataset():
    X = np.array([[10 * random.random() - 5 for _ in range(500)] for i in range(2)])
    Y = np.sin(X[0]) + 0.5*np.sin(X[0] + X[1])+ 0.2*X[1] + [0.1 * random.random() for _ in range(500)]
    ax = plt.axes(projection='3d')
    ax.scatter3D(X[0], X[1], Y, c=Y, cmap='Greens');
    plt.show()
    return np.squeeze(X.T), Y.reshape(-1, 1)


def build_model():
    model = keras.Sequential([
        layers.Dense(64, activation='relu', input_shape=(2,)),
        layers.Dense(64, activation='relu'),
        layers.Dense(1)
    ])

    optimizer = tf.keras.optimizers.Adam()
    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'mse'])
    return model


def main():
    EPOCHS = 300
    X, Y = generate_and_display_dataset()
    model = build_model()
    model.summary()

    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=10)
    history = model.fit(
        X, Y,
        epochs=EPOCHS, validation_split = 0.2, verbose = 0,
        callbacks=[early_stop, tfdocs.modeling.EpochDots()]
    )
    
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch
    print(hist.tail())

    prediction = model.predict(X)
    ax = plt.axes(projection='3d')
    ax.scatter3D(X[:,0], X[:,1], prediction, cmap='Greens');
    ax.plot_trisurf(X[:,0], X[:,1], prediction.reshape((500,)), cmap="viridis", edgecolor="none")
    plt.show()
    return 1


if __name__ == "__main__":
    main()
