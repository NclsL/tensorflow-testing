import os
import random
import pathlib
import numpy as np
import pandas as pd
import seaborn as sns
import scipy.io as sio
import matplotlib.pyplot as plt

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow_docs as tfdocs
import tensorflow_docs.plots
import tensorflow_docs.modeling

def fetch_dataset():
    path_to_current_dir = os.getcwd()
    data_dir = os.path.join(path_to_current_dir, 'data')
    test_data = sio.loadmat(os.path.join(data_dir, 'test_data.mat'))
    train_data = sio.loadmat(os.path.join(data_dir, 'train_data.mat'))

    # Create a pandas dataframe from raw data. Shuffle it so we dont
    # end up with similar graph to what is in matlab
    raw_test_data = test_data['testdata']
    test_data = pd.DataFrame(raw_test_data).T
    test_data.columns = ["Electricity", "kettle", "fridge", "washer"]

    raw_train_data = train_data['traindata']
    train_data = pd.DataFrame(raw_train_data).T
    train_data.columns = ["Electricity", "kettle", "fridge", "washer"]

    # We want to predict electricity so remove it
    train_label = train_data.pop("Electricity")
    test_label = test_data.pop("Electricity")
    return train_data, test_data, train_label, test_label


def build_model(train_data):
    model = keras.Sequential([
        layers.Dense(64, activation='relu', input_shape=[len(train_data.keys())]),
        layers.Dense(64, activation='relu'),
        layers.Dense(1)
    ])

    optimizer = tf.keras.optimizers.Adam()
    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'mse'])
    return model


def main():
    EPOCHS = 300
    train_data, test_data, train_label, test_label = fetch_dataset()
    model = build_model(train_data)
    model.summary()

    early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)
    history = model.fit(
        train_data, train_label,
        epochs=EPOCHS, validation_split = 0.2, verbose = 1,
        callbacks=[early_stop, tfdocs.modeling.EpochDots()]
    )
    
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch
    prediction = model.predict(test_data)

    ax = plt.axes()
    ax.plot(prediction[:100], "r-", label="electricity prediction n=100")
    ax.plot(test_data["kettle"][:100], "-", label="kettle")
    ax.plot(test_data["fridge"][:100], "-", label="fridge")
    ax.plot(test_data["washer"][:100], "-", label="washer")
    plt.legend()
    plt.show()
    return 1


if __name__ == "__main__":
    main()
