regression1 will create a dataset of 500 points that follows a plane with some randomness, data is fed to nn which will learn the pattern and create similar

regression2 will read .mat files from /data where 3 items have different electricity consumption, nn will learn the electricity consumption patterns that each combination devices might have

classification1 will use same /data, but this time data is given 2^3 labels, from 0..7 (integer labelled). use sparse\_categorical\_crossentropy and each electricity consumption pattern will be classified in category
